import sqlite3
import pandas as pd
import numpy as np
import datetime
import math

#Detections
#Input: 
#   pbIDstr: 
#       Buoy ID in string format
#       Must be in format "pbXXX"
#   dbLoc:
#       full path to SQLite database
#          Must contain Network_Reciever table with buoy communication from buoy of interest
#          Must contain HydrophoneStreamers table with accurate buoy name and coordinates
#          Must contain Right_Whale table with detections from buoy of interest (and associated bearings)
#          Must contain Clip_Noise_Measurement table with noise measurements of clips for the buoy of interest
#          Must contain NARW_UDP_Classifier table with classifier scores of clips for the buoy of interest
#
#Fields:
#   pbIDstr:
#       Assigned at construction
#       string representation of buoy id: "pbXXX"
#   pbNumStr:
#       Assigned at construction
#       String representation of buoy number: "XXX"
#       Network_Reciever table refers to buoy in this manner
#   dbConn:
#       Assigned at construction
#       Active database connection to the database located at dbLoc (parameter)
#       
#Will adjust to query based on time constraint so to not query all the data for the buoy

class detections:
    def __init__(self,pbIDstr,dbLoc):
        self.pbIDstr = pbIDstr
        self.pbNumStr = pbIDstr[2:]
        self.dbConn = self.connectDataBase(dbLoc)
        self.channelMapDict,self.coords = self.getBuoyData()
        self.filteredDetectionDf = self.organizeBuoyDetections()

    def connectDataBase(self,dbLoc):
        try:
            conn = sqlite3.connect(dbLoc)
            print("Opened database successfully")
        except:
            print("Could not connect to database")
            quit()
        return conn

        
    def getBuoyData(self):
        try:
            cursor = self.dbConn.execute("""SELECT Network_Receiver.ChannelBitmap,HydrophoneStreamers.Latitude,HydrophoneStreamers.Longitude,Network_Receiver.BuoyId1 
                                        FROM Network_Receiver 
                                        JOIN HydrophoneStreamers on substr(HydrophoneStreamers.Name,6,8) == Network_Receiver.BuoyId1 
                                        WHERE Network_Receiver.BuoyId1 == """+str(self.pbNumStr))
        except:
            print("Could not execute buoy data query.")
            quit()
        netRxMap = 0
        for row in cursor:
            netRxMap = row[0]
            if row[1]!=0 and row[2]!=0:
                lat = row[1]
                lon = row[2]
        RWEdgechannelBitmap = netRxMap & (netRxMap>>1)
        clipMap = RWEdgechannelBitmap & (RWEdgechannelBitmap>>1) & (RWEdgechannelBitmap>>2)
        print("NetRx channel Bitmap for "+self.pbIDstr+" is "+str(netRxMap)+ ", RWEdgeChannelBitmap:" +str(RWEdgechannelBitmap) +", Latitude: "+str(lat)+", Longitude: "+str(lon))
        return {"netRx":netRxMap,"RW":RWEdgechannelBitmap,"clip":clipMap},[lat,lon]

    
    def organizeBuoyDetections(self):
        try:
            filterQuery = "SELECT UTC,Angle_0 FROM Right_Whale WHERE ChannelBitmap == "+str(self.channelMapDict["RW"])
        except:
            print("Could not execute right whale data query")
            quit()
        filteredDetectionDf = pd.read_sql_query(filterQuery, self.dbConn)
        filteredDetectionDf["UTC"] = pd.to_datetime(filteredDetectionDf["UTC"])

        filteredDetectionDf = self.attachClips(filteredDetectionDf)
        self.filteredDetectionDf = filteredDetectionDf
        print("Gathered Filtered Detections: ")
        print(filteredDetectionDf)
        return filteredDetectionDf
    def attachClips(self,filteredDetectionDf):
        clipMeasurementQuery = """SELECT Clip_Noise_Measurement.SNR as SNR,Clip_Noise_Measurement.UTC as UTC,NARW_UDP_Classifier.Score as ClassScore
                                FROM Clip_Noise_Measurement 
                                INNER JOIN NARW_UDP_Classifier on NARW_UDP_Classifier.Clip_UID==Clip_Noise_Measurement.Clip_UID
                                WHERE NetRxChanMap == """+str(self.channelMapDict["netRx"])
        
        try:
            SNRDf = pd.read_sql_query(clipMeasurementQuery, self.dbConn)
        except:
            print("Could not execute clip measurements data query")
            quit()
        SNRDf["UTC"] = pd.to_datetime(SNRDf["UTC"])
        print("Clip Data Loaded:")
        print(SNRDf)
        SNRDf["UTC"] = SNRDf["UTC"] + pd.to_timedelta(1, unit='s')
        combinedDf = filteredDetectionDf.join(SNRDf.set_index('UTC'), on='UTC')
        return combinedDf


    def matchPlaybacks(self,playbacksDf):
        self.filteredDetectionDf = self.filteredDetectionDf.reset_index()
        self.filteredDetectionDf["InSession"] = 0
        self.filteredDetectionDf["srcLat"] = 0
        self.filteredDetectionDf["srcLon"] = 0
        self.filteredDetectionDf["TrueBearing"] = 0
        for i,row in self.filteredDetectionDf.iterrows():
            difCol = abs(playbacksDf["UTC"] - row["UTC"])
            minVal = difCol.min()
            minIdx = difCol.idxmin()
            if minVal<datetime.timedelta(seconds=4):
                srcLat = playbacksDf.loc[minIdx,"Lat"]
                srcLon = playbacksDf.loc[minIdx,"Lon"]
                self.filteredDetectionDf.loc[i,"srcLat"] = srcLat
                self.filteredDetectionDf.loc[i,"srcLon"] = srcLon
                self.filteredDetectionDf.loc[i,"TrueBearing"] = self.calcBearing(srcLat,srcLon)
                self.filteredDetectionDf.loc[i,"InSession"] = 1
        self.filteredDetectionDf = self.filteredDetectionDf[self.filteredDetectionDf["InSession"]==1]
        self.filteredDetectionDf["TrueBearing"] = self.filteredDetectionDf.apply(lambda row : self.boundAngle(row["TrueBearing"]),axis=1)
        self.filteredDetectionDf["AngleDif"] = self.filteredDetectionDf["TrueBearing"] -self.filteredDetectionDf["Angle_0"]
        self.filteredDetectionDf["AngleDif"] = self.filteredDetectionDf.apply(lambda row : self.boundAngle(row["AngleDif"]),axis=1)
        self.filteredDetectionDf["AngleDifDeg"] = self.filteredDetectionDf["AngleDif"]*180/math.pi
        self.filteredDetectionDf = self.filteredDetectionDf[self.filteredDetectionDf["ClassScore"]>.9]
        print("playbacks matched: ")
        print(self.filteredDetectionDf)
        return self.filteredDetectionDf

    def boundAngle(self,theta):
        while theta<math.pi*-1:
            theta+=math.pi*2
        while theta>math.pi:
            theta-=math.pi*2
        return theta
    def calcBearing(self,lat,lon):
        lambda1 = self.coords[1]*math.pi/180
        phi1 = self.coords[0]*math.pi/180
        lambda2 = lon*math.pi/180
        phi2 = lat*math.pi/180
        dlambda = abs(lambda2-lambda1)%180
        dphi = math.log(math.tan(phi2/2+math.pi/4)/math.tan(phi1/2+math.pi/4))
        return math.atan2(dlambda,dphi)#-math.pi/2
