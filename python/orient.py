import pandas as pd
import numpy as np
from filteredObjects.detections import detections
from filteredObjects.playbacks import playbacks
import plotResults
import sys
import seaborn as sns
from matplotlib import pyplot as plt

#Module to call when executing orientation script
#   For use with SMRU CABOW for Right Whales orientation
#
#
#Input: 
#  Command param [1]: Full buoy ID
#           Required
#           Format: "pbXXX"
#  Comand param [2]: Buoy Offset Bounds
#           Optional. If not provided bounds will be set to median+/-StdDev
#           Format: "doubleA,doubleB"
#  Not on command: SQLite Database Location:
#           Required. Parameter hard-coded as "receiveddbLoc" (line XX)
#           Database must be created with CABOW Base Station Config: CABOW_BASE_20210713.psfx
#  Not on command: Playbacks data location
#           Required. Parameter hard-coded as "playbacksDataBase" (line XX)
#           Directory format:
#               <base_dir>
#                   pbXXX
#                       playbacks_meta.csv
#
#Output:
#   All output sent to playbacksDataBase+\pbID\
#   FullFig.png
#       Initial detection and playbacks rose plot
#       Bound detections offset and playbacks rose plot
#       Lat/Long of buoy and playbacks plot
#       Angle Offset vs. SNR Plot
#   MatchedPlaybacksDetections.csv
#       Merged table of detection and clip data with playbacks data
#   Results.txt
#       Timestamped results from orientation calibration


#Get buoy ID from the execute command
pbID = sys.argv[1]

#Define hardcoded directory and database parameters
receiveddbLoc = "C:\\Users\\12066\\Documents\\CABOW\\East Coast Field Trials\\GRIFFINBAY_20210727\\DataBase\\Deployment\\Backups\\GRIFFINBAY_20210728_FINAL_COPY.sqlite3"
playbacksDataBase = "C:\\Users\\12066\\Documents\\CABOW\\East Coast Field Trials\\GRIFFINBAY_20210727\\Orientation\\"
playbacksDataLoc = playbacksDataBase+pbID+"\\"
playbacksInputLocation = playbacksDataLoc+"playbacks_meta.csv"

#Create instance of detections object for Buoy of interest
try:
    BuoyDetections = detections(pbID,receiveddbLoc)
except:
    print("Could not generate detections object. Ensure database is configured correctly.")
    quit()

#Create instance of playbacks object from playbacks csv
try:
    filteredPlaybacks = playbacks(playbacksInputLocation)
except:
    print("Could not generate playbacks object. Ensure you have the right csv format.")
    quit()

#Match playback locations with detections and generate offset angle data
try:
    matchedPlaybacksDf = BuoyDetections.matchPlaybacks(filteredPlaybacks.playbacksDF)
except:
    print("Could not match playbacks and detections. Ensure detections exist for the timeframe identified in the playbacks metadata.")
    quit()

#Search for manual boundary conditions
try:
    #If they exist attempt to assign manualBounds with values
    manualBoundsStr = sys.argv[2]
    manualBounds = manualBoundsStr.split(",")
    manualBounds[0] = float(manualBounds[0])
    manualBounds[1] = float(manualBounds[1])
    #Call plot function for generating output
    plotResults.createPlots(matchedPlaybacksDf,BuoyDetections,playbacksDataLoc,manualBounds=manualBounds)
except:
    plotResults.createPlots(matchedPlaybacksDf,BuoyDetections,playbacksDataLoc)
    print("No valid manual bounds detected")

matchedPlaybacksDf.to_csv(playbacksDataLoc+pbID+"MatchedPlaybacksDetections.csv")